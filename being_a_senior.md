# What mindset makes a senior developer (or what I learned while becoming one)

# Code you write

## Write boring code.

I used to believe you have to always use most up-to date features language may offer, but coding for the sake of less and pretty code always resulted in poor maintainability. Some Next guy should not spend time with additional complexity you create. Even you will most like get lost in your fancy code after a month or two. Long function names and descriptive variables, more comments - all is appreciated.

JavaScript, for example, offers you a lot of options to do same thing. Consider following:

```typescript
// 1
const init = async function initDataset() {...}

// 2
const init = async () => {...}
```

First function declaration is way more wordy, but it has a set of advantages, especially when reading stacktraces. So in debug-intensive applications I would consider first way over second one.

Your enemy is always the complexity. If it is complex, it is wrong. Previuosly, I was proud of building enormously complex solutions, which were hard to support myself after a month or two.

That brings us to the following conclusion:
*Code should be written for humans to read, and eventually for machines to execute. "Surprises" in text harm comprehension*

//TBD

# Dealing with pressure

Pressure makes diamomds. And also pushes people away. Be sure to quit job before you become a diamond. Spend time with family.
After working with technically bankrupt projects with tremendous pressure to get stuff done, I discovered that the more pressure you get, the less value you can bring. It leads to burnout, neglection of coding standards and forfeiting quality checks.

Write good code.
Or quit.

# With time, new programming languages are faster to learn.

From C/C++ to Delphi, Java and Ruby, then Python and Javascript, I was working with many different languages. But core concepts still remain the same. There will always be an abstraction for thread, file, memory, process... Language just interacts with them, so it is easy to work if you know abstractions. Also, modern languages share same set of ideas, like function, class, module, library... and many more. They take and learn from each other, so every next language learned will surprise you less.

# Package applications properly

I used to write 'works-for-me' applications a while ago. But execution environment of end user/server may be totally unexpected. Some variables/libraries/packages may be missing or work incorrectly.
That's why applications should carry all depencencies with them. There are some solutions like that:
* Docker
* Snap
* npm, pip, maven... 

You may even decide to static-compile application to get all dependencies inside.

Rule of thumb - if it requires something - this something will be missing.


# Always have scale in mind

At first, I was thinking on 